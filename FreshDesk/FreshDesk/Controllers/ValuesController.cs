﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FreshDesk.Model;
using FreshDesk.Repositories;

namespace FreshDesk.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {

        private readonly ISynchRepository _synchronize;
        private readonly FreshDeskContext _db;
        public ValuesController(FreshDeskContext db, ISynchRepository synchronize)
        {
            _synchronize = synchronize;
            _db = db;
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var urls = _db.GitUrls.Where(x => x.FreshDeskId == id);
             
            return Ok(
                    new { values =urls}
                   );
        }

        // POST api/values
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Ticket ticket)
        {
            if (ticket == null)
                return BadRequest();

            var result = await _synchronize.SynchronizeAsync(ticket);

            var git = new GitUrl(ticket.id,result.id,result.web_url);
            _db.GitUrls.Add(git);
            _db.SaveChanges();
            return Ok(
                result
            );
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
