﻿using FreshDesk.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreshDesk
{
    public class FreshDeskContext : DbContext
    {
        public DbSet<GitUrl> GitUrls { get; set; }

        public FreshDeskContext(DbContextOptions<FreshDeskContext> options):base(options)
        {   }

        
        protected override void OnModelCreating(ModelBuilder builder)
        {
            var giturl = builder.Entity<GitUrl>();
            giturl.HasKey(x => new { x.FreshDeskId, x.GitLabId });
            base.OnModelCreating(builder);
        }

    }
}
