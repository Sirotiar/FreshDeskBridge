﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreshDesk.Model
{
    public class GitUrl
    {
        public int FreshDeskId { get; set; }
        public int GitLabId { get; set; }
        public string Url { get; set; }

        public GitUrl(int freshDeskID,int gitLabID,string url)
        {
            FreshDeskId = freshDeskID;
            GitLabId = gitLabID;
            Url = url;
        }
        public GitUrl()
        {
        }
    }
}
