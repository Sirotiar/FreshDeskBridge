﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreshDesk.Model
{
    public class Ticket
    {
        public List<string> cc_emails { get; set; }
        public List<object> fwd_emails { get; set; }
        public List<string> reply_cc_emails { get; set; }
        public bool fr_escalated { get; set; }
        public bool spam { get; set; }
        public object email_config_id { get; set; }
        public object group_id { get; set; }
        public int priority { get; set; }
        public long requester_id { get; set; }
        public object responder_id { get; set; }
        public int source { get; set; }
        public object company_id { get; set; }
        public int status { get; set; }
        public string subject { get; set; }
        public object to_emails { get; set; }
        public object product_id { get; set; }
        public int id { get; set; }
        public object type { get; set; }
        public string due_by { get; set; }
        public string fr_due_by { get; set; }
        public bool is_escalated { get; set; }
        public string description { get; set; }
        public string description_text { get; set; }
        public CustomFields custom_fields { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public List<object> tags { get; set; }
        public List<object> attachments { get; set; }
    }


    public class Attachment
    {
        public long id { get; set; }
        public string content_type { get; set; }
        public int size { get; set; }
        public string name { get; set; }
        public string attachment_url { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
    }

    public class CustomFields
    {
        public object git { get; set; }
    }
}
