﻿using FreshDesk.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace FreshDesk.Repositories
{
    public interface ISynchRepository
    {
        Task<GitLabIssue> SynchronizeAsync(Ticket ticket);
    }

    public class GitlabSynchRepository : ISynchRepository
    {
        private readonly int _projectID;
        private readonly string _privateToken;
        public GitlabSynchRepository()
        {
            //test
            _projectID = 20;
            _privateToken = "i2NCLCXwDh96nFAkEsGP";
            //
          //  _projectID = 1;
          //  _privateToken = "yH4kAJL7NEQ8sRzhfXLN";
        }

        public async Task<GitLabIssue> SynchronizeAsync(Ticket ticket)
        {

            var response = await CreateGitLabIssue(ticket);
            return response;

        }

        private async Task<GitLabIssue> CreateGitLabIssue(Ticket ticket)
        {
            var title = $"[{ticket.id}] {ticket.subject}";
            using (var client = new HttpClient())
            {
                string requestUrl = $"https://code.evolio.cz/api/v4/projects/{_projectID}/issues?";

                string description = string.Format("https://avesoft.freshdesk.com/helpdesk/tickets/{0}\n\n {1}", ticket.id, ticket.description_text);
                var data = new List<KeyValuePair<string, string>>();
                data.Add(new KeyValuePair<string, string>("title", title));
                data.Add(new KeyValuePair<string, string>("private_token", _privateToken));
                data.Add(new KeyValuePair<string, string>("labels", "FreshDesk"));
                data.Add(new KeyValuePair<string, string>("description", description));
                data.Add(new KeyValuePair<string, string>("created_at", ticket.created_at));
                data.Add(new KeyValuePair<string, string>("weight", (ticket.status * 2).ToString()));

                HttpContent content = new FormUrlEncodedContent(data);

                var response = await client.PostAsync(requestUrl, content);
                response.EnsureSuccessStatusCode();
                var JSON = await response.Content.ReadAsStringAsync();

                var jsonResponse = JsonConvert.DeserializeObject<GitLabIssue>(JSON);
                return jsonResponse;
            }


        }

        private async Task<IEnumerable<GitLabIssue>> GetGitLabIssue()
        {

            using (var client = new HttpClient())
            {
                var response = await client.GetAsync(string.Format("https://code.evolio.cz/api/v4/projects/{0}/issues?private_token={1}&labels=FreshDesk", _projectID, _privateToken));
                response.EnsureSuccessStatusCode();
                var JSON = await response.Content.ReadAsStringAsync();

                var jsonResponse = JsonConvert.DeserializeObject<IEnumerable<GitLabIssue>>(JSON);
                return jsonResponse;
            }
        }

        private GitLabIssue GetCurrentIssue(int ticketID, IEnumerable<GitLabIssue> Issues)
        {
            foreach (var issue in Issues)
            {
                var IdIssueGitLab = issue.title.Split('-').FirstOrDefault();

                if (ticketID.ToString() == IdIssueGitLab)
                    return issue;
            }

            return null;
        }

    }
}
